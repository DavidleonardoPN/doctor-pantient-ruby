module Entities
  class DoctorEntity < Grape::Entity
    expose :id
    expose :name
    expose :phone
    expose :email
  end
end
