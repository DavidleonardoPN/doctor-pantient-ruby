module Entities
  class PatientEntity < Grape::Entity
    expose :id
    expose :name
    expose :phone
    expose :email
  end
end
