require 'doorkeeper/grape/helpers'
module API
  module V1
    class Patients < Grape::API
      include API::V1::Defaults
      helpers Doorkeeper::Grape::Helpers


      helpers do
        def clean_params(params)
          ActionController::Parameters.new(params)
        end
      end

      before do
        doorkeeper_authorize!
      end


      resource :patients do

        desc "Return all patients"
        get "", root: :patients do
          @Patients = Patient.all

          if @Patients.present?
            render data: @Patients
          else
            error!("Sorry, data not found", 422)
          end
        end

        post "/create" do
          patient_params = clean_params(params[:patient]).permit(:name, :phone, :email, :password)

          @patient =   Patient.new(patient_params)

          if @patient.save
            { message: "success" }
          else
            error!(@patient.errors.full_messages.join("<br />").html_safe, 422)
          end

        end

        route_param :id do
          get "/appointment_list"  do
            @Appointments = Patient.find(params[:id]).appointments.order(:status)

            if @Appointments.present?
              render data: @Appointments
            else
              error!("Sorry, data not found", 422)
            end
          end

          get "/show"  do
            @patient = Patient.find(params[:id])
            present @patient
          end

          put "/update" do
            patient_params = clean_params(params[:patient]).permit(:name, :phone, :email, :password)

            @patient =  Patient.find(params[:id])

            if @patient.update(patient_params)
              { message: "success" }
            else
              error!(@patient.errors.full_messages.join("<br />").html_safe, 422)
            end
          end

          delete "/destroy"  do
            patient = Patient.find(params[:id])
            patient.destroy
            { message: "success to destroy" }
          end
        end



      end

    end
  end
end
