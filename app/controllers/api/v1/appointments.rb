module API
  module V1
    class Appointments < Grape::API
      include API::V1::Defaults
      helpers Doorkeeper::Grape::Helpers

      helpers do
        def clean_params(params)
          ActionController::Parameters.new(params)
        end
      end

      before do
        doorkeeper_authorize!
      end

      resource :appointments do

      
        post "/create" do
          appointment_params = clean_params(params[:appointment]).permit(:doctor_id, :patient_id)

          @appointment =   Appointment.new(appointment_params.merge({date: DateTime.now.to_date, :status => "Need Approval"}))

          if @appointment.save
            { message: "success" }
          else
            error!( @appointment.errors.full_messages.join("<br />").html_safe, 422)
          end

        end

        route_param :id do
          get "/approval" do

            @appointment =  Appointment.find(params[:id])

            if @appointment.update(status: 'approval')
              { message: "success" }
            else
              error!(@appointment.errors.full_messages.join("<br />").html_safe, 422)
            end
          end
        end


      end

    end
  end
end
