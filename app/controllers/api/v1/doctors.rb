require 'doorkeeper/grape/helpers'
module API
  module V1
    class Doctors < Grape::API
      auth :grape_devise_token_auth, resource_class: :user
      include API::V1::Defaults
      helpers Doorkeeper::Grape::Helpers
      helpers GrapeDeviseTokenAuth::AuthHelpers

      helpers do
        def clean_params(params)
          ActionController::Parameters.new(params)
        end
      end

      before do
        doorkeeper_authorize!
      end

      resource :doctors do

        desc "Return all doctor"
        get "", root: :doctors do

          @Doctors = Doctor.all

          if @Doctors.present?
            render data: @Doctors
          else
            error!("Sorry, Data not found", 422)
          end
        end

        post "/create" do
          doctor_params = clean_params(params[:doctor]).permit(:name, :phone, :email, :password)

          @doctor =   Doctor.new(doctor_params)

          if @doctor.save
            { message: "success" }
          else
            error!(@doctor.errors.full_messages.join("<br />").html_safe, 422)
          end

        end


        route_param :id do
          desc "Return all appointment"
          get "/appointment_list"  do
            @Appointments = Doctor.find(params[:id]).appointments.order(:status)

            if @Appointments.present?
              render data: @Appointments
            else
              error!("Sorry, data not found", 422)
            end
          end


          put "/update" do
            doctor_params = clean_params(params[:doctor]).permit(:name, :phone, :email, :password)

            @doctor =  Doctor.find(params[:id])

            if @doctor.update(doctor_params)
              { message: "success" }
            else
              error!(@doctor.errors.full_messages.join("<br />").html_safe, 422)
            end
          end

          get "/show"  do
            @doctor = Doctor.find(params[:id])
            present @doctor, with: Entities::DoctorEntity
          end

          put "/update" do
            doctor_params = clean_params(params[:doctor]).permit(:name, :phone, :email, :password)

            @doctor =  Doctor.find(params[:id])

            if @doctor.update(doctor_params)
              { message: "success" }
            else
              error!(@doctor.errors.full_messages.join("<br />").html_safe, 422)
            end
          end
        end



      end

    end
  end
end
