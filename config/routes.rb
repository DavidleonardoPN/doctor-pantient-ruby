Rails.application.routes.draw do
  devise_for :patients
  devise_for :doctors

  use_doorkeeper
  # devise_for :patients
  # devise_for :doctors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 mount API::Base, at: "/"
 mount GrapeSwaggerRails::Engine, at: "/documentation"

 root to: "home#index"
end
