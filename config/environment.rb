
#Encoding.default_internal, Encoding.default_external = ['utf-8'] * 2
require File.expand_path('../application', __FILE__)

# Initialize the rails application
TestApi::Application.initialize!

# NewRelic::Agent.manual_start :app_name => 'ReadyFlight', :agent_enabled => true

# Date::DATE_FORMATS[:default] = "%m/%d/%Y"

if Rack::Utils.respond_to?("key_space_limit=")
  Rack::Utils.key_space_limit = 2621440
end
